﻿using Exercise1;
using System.Collections;
using System.Text;

Console.OutputEncoding = Encoding.Unicode;

ArrayList arrayList = new ArrayList();
arrayList.Add("Hieu");
arrayList.Add("Hoang");
arrayList.Add(1);
arrayList.Add(2);
arrayList.Add("Trong");
arrayList.Add(3.9d);

ArrayListExtension extension = new ArrayListExtension();

int countInt = extension.CountInt(arrayList);
int countOfString = extension.CountOf(arrayList,typeof(string));
int countIntGeneric = extension.CountOf<int>(arrayList);
int maxInt = extension.MaxOf<int>(arrayList);

Console.WriteLine("CountInt() nên trả về: " + countInt);
Console.WriteLine("CountOf(typeof(string)) nên trả về: " + countOfString);
Console.WriteLine("CountOf<int>() nên trả về: " + countIntGeneric);
Console.WriteLine("MaxOf<int>() nên trả về: " + maxInt);



int countIntStatic = arrayList.CountInt();
int countOfStringStatic = arrayList.CountOf(typeof(string));
int countIntGenericStatic = arrayList.CountOf<int>();
int maxIntStatic = arrayList.MaxOf<int>();

Console.WriteLine("CountInt() nên trả về: " + countIntStatic);
Console.WriteLine("CountOf(typeof(string)) nên trả về: " + countOfStringStatic);
Console.WriteLine("CountOf<int>() nên trả về: " + countIntGenericStatic);
Console.WriteLine("MaxOf<int>() nên trả về: " + maxIntStatic);