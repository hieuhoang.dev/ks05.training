﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLySinhVien
{

    internal class QuanLy
    {
        public List<SinhVien> danhSachSinhVien = new List<SinhVien>();


        public int TuTangMaSinhVien()
        {
            int tongSinhVien = danhSachSinhVien.Count();
            int maSinhVien = 1;

            if (tongSinhVien > 0)
            {
                maSinhVien = danhSachSinhVien[tongSinhVien - 1].ma + 1;
            }

            return maSinhVien;
        }

        public void ThemSinhVien()
        {
            //Console.InputEncoding = Encoding.UTF8;
            SinhVien sinhVien = new SinhVien();
            sinhVien.ma = TuTangMaSinhVien();
            Console.WriteLine("Mời bạn nhập tên sinh viên: ");
            sinhVien.tenSinhVien = Convert.ToString(Console.ReadLine());
            Console.WriteLine("Mời bạn nhập giới tính sinh viên: ");
            sinhVien.gioiTinh = Convert.ToString(Console.ReadLine());
            Console.WriteLine("Mời bạn nhập tuổi sinh viên: ");
            sinhVien.tuoi = int.Parse(Console.ReadLine());
            Console.WriteLine("Mời bạn nhập điểm toán sinh viên: ");
            sinhVien.diemToan = float.Parse(Console.ReadLine());
            Console.WriteLine("Mời bạn nhập điểm lý sinh viên: ");
            sinhVien.diemLy = float.Parse(Console.ReadLine());
            Console.WriteLine("Mời bạn nhập điểm hóa sinh viên: ");
            sinhVien.diemHoa = float.Parse(Console.ReadLine());

            danhSachSinhVien.Add(sinhVien);

            Console.WriteLine("Bạn có muốn nhập tiếp hay không?");
            string check = Console.ReadLine();

            if (check.Equals("Y") || check.Equals("y"))
            {
                ThemSinhVien();
            }

        }

        public void HienThiDanhSachSinhVien()
        {
            float diemTrungBinh;
            string hocLuc = "";
            foreach (SinhVien sinhVien in danhSachSinhVien)
            {
                diemTrungBinh = (sinhVien.diemToan + sinhVien.diemLy + sinhVien.diemHoa) / 3;

                if (diemTrungBinh >= 8)
                {
                    hocLuc = "Giỏi";
                }
                else if (diemTrungBinh >= 6.5 && diemTrungBinh < 8)
                {
                    hocLuc = "Khá";
                }
                else if (diemTrungBinh >= 5 && diemTrungBinh < 6.5)
                {
                    hocLuc = "Trung Bình";
                }
                else
                {
                    hocLuc = "Yếu";
                }

                Console.WriteLine(sinhVien.ma
                        + " | " + sinhVien.tenSinhVien
                        + " | " + sinhVien.gioiTinh
                        + " | " + sinhVien.tuoi
                        + " | " + sinhVien.diemToan
                        + " | " + sinhVien.diemLy
                        + " | " + sinhVien.diemHoa
                        + " | " + diemTrungBinh
                        + " | " + hocLuc);
            }
        }

        public void CapNhatSinhVien()
        {
            Console.WriteLine("Nhập mã sinh viên cần cập nhật: ");
            int maCapNhat = int.Parse(Console.ReadLine());
            int check = 0;
            foreach (SinhVien sinhVien in danhSachSinhVien)
            {
                if (sinhVien.ma == maCapNhat)
                {
                    Console.WriteLine("Mời bạn nhập tên sinh viên: ");
                    sinhVien.tenSinhVien = Console.ReadLine();
                    Console.WriteLine("Mời bạn nhập giới tính sinh viên: ");
                    sinhVien.gioiTinh = Console.ReadLine();
                    Console.WriteLine("Mời bạn nhập tuổi sinh viên: ");
                    sinhVien.tuoi = int.Parse(Console.ReadLine());
                    Console.WriteLine("Mời bạn nhập điểm toán sinh viên: ");
                    sinhVien.diemToan = float.Parse(Console.ReadLine());
                    Console.WriteLine("Mời bạn nhập điểm lý sinh viên: ");
                    sinhVien.diemLy = float.Parse(Console.ReadLine());
                    Console.WriteLine("Mời bạn nhập điểm hóa sinh viên: ");
                    sinhVien.diemHoa = float.Parse(Console.ReadLine());
                    check = 1;
                }
            }
            if(check == 0)
            {
                Console.WriteLine("Không tìm thấy sinh viên");
            }
        }

        public void XoaSinhVien()
        {
            Console.WriteLine("Nhập mã sinh viên cần xóa: ");
            int maCanXoa = int.Parse(Console.ReadLine());
            int check = 0;
            SinhVien sinhVienCanXoa = new SinhVien();
            foreach (SinhVien sinhVien in danhSachSinhVien)
            {
                if (sinhVien.ma == maCanXoa)
                {
                    sinhVienCanXoa = sinhVien;
                    check = 1;
                }
            }
            danhSachSinhVien.Remove(sinhVienCanXoa);
            if (check == 0)
            {
                Console.WriteLine("Không tìm thấy sinh viên");
            }
        }

        public void TimKiemTheoTen()
        {
            Console.OutputEncoding = Encoding.UTF8;

            Console.Write("Nhập tên cần tìm: ");
            string Ten = Console.ReadLine();
            foreach(SinhVien sinhVien in danhSachSinhVien)
            {
                if (sinhVien.tenSinhVien.Contains(Ten))
                {
                    Console.WriteLine(sinhVien.ma
                        + " | " + sinhVien.tenSinhVien
                        + " | " + sinhVien.gioiTinh
                        + " | " + sinhVien.tuoi
                        + " | " + sinhVien.diemToan
                        + " | " + sinhVien.diemLy
                        + " | " + sinhVien.diemHoa);
                }
            }

        }

        public void SapXepTheoTrungBinh()
        {
            List<SinhVien> danhSachMoi = new List<SinhVien>();
            for(int i = 0; i < danhSachSinhVien.Count; i++)
            {
                danhSachMoi.Add(danhSachSinhVien[i]);
            }
            double diemTB1;
            double diemTB2;
            SinhVien sinhVienTemp = new SinhVien();
            for(int i = 0; i < danhSachSinhVien.Count; i++)
            {
                diemTB1 = (danhSachMoi[i].diemLy + danhSachMoi[i].diemToan + danhSachMoi[i].diemHoa) / 3;
                for (int j = i + 1; j < danhSachSinhVien.Count; j++)
                {
                    diemTB2 = (danhSachMoi[j].diemLy + danhSachMoi[j].diemToan + danhSachMoi[j].diemHoa) / 3;
                    if (diemTB1 > diemTB2)
                    {
                        sinhVienTemp = danhSachMoi[i];
                        danhSachMoi[i] = danhSachMoi[j];
                        danhSachMoi[j] = sinhVienTemp;
                    }
                }
            }
            foreach(SinhVien sinhVien in danhSachMoi)
            {
                Console.WriteLine(sinhVien.ma
                        + " | " + sinhVien.tenSinhVien
                        + " | " + sinhVien.gioiTinh
                        + " | " + sinhVien.tuoi
                        + " | " + sinhVien.diemToan
                        + " | " + sinhVien.diemLy
                        + " | " + sinhVien.diemHoa);
            }
        }

        public void SapXepTheoTen()
        {
            List<SinhVien> danhSachMoi = new List<SinhVien>();
            for (int i = 0; i < danhSachSinhVien.Count; i++)
            {
                danhSachMoi.Add(danhSachSinhVien[i]);
            }


            SinhVien sinhVienTemp = new SinhVien();
            for (int i = 0; i < danhSachMoi.Count; i++)
            {
                
                for (int j = i + 1; j < danhSachMoi.Count; j++)
                {
                    if (danhSachMoi[i].tenSinhVien.CompareTo(danhSachMoi[j].tenSinhVien)>0)
                    {
                        sinhVienTemp = danhSachMoi[i];
                        danhSachMoi[i] = danhSachMoi[j];
                        danhSachMoi[j] = sinhVienTemp;
                    }
                }
            }
            foreach (SinhVien sinhVien in danhSachMoi)
            {
                Console.WriteLine(sinhVien.ma
                        + " | " + sinhVien.tenSinhVien
                        + " | " + sinhVien.gioiTinh
                        + " | " + sinhVien.tuoi
                        + " | " + sinhVien.diemToan
                        + " | " + sinhVien.diemLy
                        + " | " + sinhVien.diemHoa);
            }
        }
    }
}
