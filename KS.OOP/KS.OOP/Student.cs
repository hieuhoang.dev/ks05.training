﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KS.OOP
{
    internal class Student
    {
        public Student()
        {
        }

        public Student(int Id, string Name)
        {
            this.Id = Id;
            this.Name = Name;
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public void GoToSchool()
        {
            Console.WriteLine("Đi hoc dai hoc");
        }
    }
}
