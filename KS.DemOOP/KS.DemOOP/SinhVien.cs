﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KS.DemOOP
{
    internal class SinhVien
    {
        public string maSV { get; set; }
        public string tenSV { get; set; }
        public int namSinh { get; set; }

        public SinhVien()
        {
        }

        public SinhVien(string maSV, string tenSV, int namSinh)
        {
            this.maSV = maSV;
            this.tenSV = tenSV;
            this.namSinh = namSinh;
        }

        public void InThongTin()
        {
            Console.WriteLine("Thông tin cha");
        }
    }
}
