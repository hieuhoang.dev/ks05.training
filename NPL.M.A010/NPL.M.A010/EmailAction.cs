﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace NPL.M.A010
{
    internal class EmailAction
    {
        OutlookMail email = new OutlookMail();
        public OutlookMail CreateNewEmail()
        {
            Console.Write("From: ");
            email.From = ReadEmailFromConsole();

            Console.Write("To (separate multiple recipients with a comma): ");
            email.To = ReadEmailListFromConsole();

            Console.Write("Cc (separate multiple recipients with a comma): ");
            email.Cc = ReadEmailListFromConsole();

            Console.Write("Subject: ");
            email.Subject = Console.ReadLine();

            Console.Write("Attachment: ");
            email.Attachment = Console.ReadLine();

            Console.WriteLine("Mail Body (Enter lines of text, type <END> on a new line to finish):");
            email.MailBody = ReadMultilineTextFromConsole();

            Console.Write("Is Important (Yes/No): ");
            string isImportantInput = Console.ReadLine();
            email.IsImportant = isImportantInput.Equals("Yes", StringComparison.OrdinalIgnoreCase);

            if (email.IsImportant)
            {
                Console.Write("Password: ");
                email.Password = Console.ReadLine();
                // You can encrypt the password here
            }

            return email;
        }

        public string ReadEmailFromConsole()
        {
            string input = Console.ReadLine();
            // Add email validation if needed
            return input;
        }

        public List<string> ReadEmailListFromConsole()
        {
            string input = Console.ReadLine();
            List<string> recipients = new List<string>(input.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries));
            return recipients;
        }

        public List<string> ReadMultilineTextFromConsole()
        {
            List<string> lines = new List<string>();
            while (true)
            {
                string line = Console.ReadLine();
                if (line == "<END>")
                    break;
                lines.Add(line);
            }
            return lines;
        }

        public void CreateFileXml()
        {
            XmlDocument xmlDoc = new XmlDocument();

            // Create the XML declaration
            XmlDeclaration xmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", "UTF-8", null);
            xmlDoc.AppendChild(xmlDeclaration);

            XmlElement outlookEmail = xmlDoc.CreateElement("OutlookEmail");
            xmlDoc.AppendChild(outlookEmail);

            // Create the root element
            XmlElement root = xmlDoc.CreateElement("Mail");
            outlookEmail.AppendChild(root);

            // Create and add elements to the root
            XmlElement element1 = xmlDoc.CreateElement("From");
            element1.InnerText = email.From;
            element1.InnerText = "2";
            root.AppendChild(element1);

            //XmlElement xmlElement = xmlDoc.CreateElement("FromFrom");
            //xmlElement.InnerText = "FromFrom";
            //element1.AppendChild(xmlElement);
            

            XmlElement element2 = xmlDoc.CreateElement("To");
            //&lt; Address & gt; chienhv1 @fsoft.com.vn & lt;/ Address & gt;
            foreach (var t in email.To)
            {
                XmlElement address = xmlDoc.CreateElement("Address");
                address.InnerText = t.ToString().Trim();
                element2.AppendChild(address);
            }
            root.AppendChild(element2);

            // Save the XML document to a file
            xmlDoc.Save("D:\\Class\\2023\\HN23_CPL_FUKS_05\\ks05.training\\NPL.M.A010\\NPL.M.A010\\FileXML\\XMLFile1.xml");

            Console.WriteLine("XML file created successfully.");
        }

        public void ReadFileXml()
        {
            String URLString = "D:\\Class\\2023\\HN23_CPL_FUKS_05\\ks05.training\\NPL.M.A010\\NPL.M.A010\\FileXML\\XMLFile1.xml";
            XmlTextReader reader = new XmlTextReader(URLString);

            while (reader.Read())
            {
                switch (reader.NodeType)
                {
                    case XmlNodeType.Element: // The node is an element.
                        Console.Write("<" + reader.Name);
                        if(reader.Name == "From")
                        {

                        }
                        while (reader.MoveToNextAttribute()) // Read the attributes.
                            Console.Write(" " + reader. Name+ "='" + reader.Value + "'");
                        Console.Write(">");
                        Console.WriteLine(">");
                        break;
                    case XmlNodeType.Text: //Display the text in each element.
                        Console.WriteLine(reader.Value);
                        break;
                    case XmlNodeType.EndElement: //Display the end of the element.
                        Console.Write("</" + reader.Name);
                        Console.WriteLine(">");
                        break;
                }
            }
        }

    }
}
