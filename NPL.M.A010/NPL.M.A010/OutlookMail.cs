﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A010
{
    public class OutlookMail
    {
        public string From { get; set; }
        public List<string> To { get; set; }
        public List<string> Cc { get; set; }
        public string Subject { get; set; }
        public string Attachment { get; set; }
        public List<string> MailBody { get; set; }
        public bool IsImportant { get; set; }
        public string Password { get; set; }
        public DateTime SentDate { get; set; }
        public string Status { get; set; }
    }
}
