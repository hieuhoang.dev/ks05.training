﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A010
{
    public static class Validate
    {
        static string ReadEmailFromConsole()
        {
            string input = Console.ReadLine();

            // Đảm bảo rằng input không rỗng và là một địa chỉ email hợp lệ
            while (string.IsNullOrWhiteSpace(input) || !IsValidEmail(input))
            {
                Console.WriteLine("Địa chỉ email không hợp lệ. Vui lòng nhập email hợp lệ:");
                input = Console.ReadLine();
            }

            return input;
        }

        static bool IsValidEmail(string email)
        {
            try
            {
                var addr = new MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }


    }
}
