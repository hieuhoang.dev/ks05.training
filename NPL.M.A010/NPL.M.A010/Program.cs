﻿using NPL.M.A010;
using System.Text;

internal class Program
{
    //static List<OutlookMail> emailList = new List<OutlookMail>();
    public static void Main(string[] args)
    {
        Console.OutputEncoding = Encoding.Unicode;
        //OutlookMail email = new OutlookMail
        //{
        //    From = "user@example.com",
        //    To = new List<string> { "recipient1@example.com", "recipient2@example.com" },
        //    Cc = new List<string> { "cc@example.com" },
        //    Subject = "Sample Email",
        //    Attachment = "path/to/attachment",
        //    MailBody = new List<string> { "Hello,", "This is a test email." },
        //    IsImportant = true,
        //    Password = "secret_password",
        //    SentDate = DateTime.Now,
        //    Status = "Draft"
        //};

        //emailList.Add(email);

        //// You can perform operations like sorting and displaying the email list here
        //SortAndDisplayEmailList();
        EmailAction emailAction = new EmailAction();
        while (true)
        {
            Console.WriteLine("Select an option:");
            Console.WriteLine("1. New Email");
            Console.WriteLine("2. Sent Emails");
            Console.WriteLine("3. Draft Emails");
            Console.WriteLine("4. Exit");

            string option = Console.ReadLine();

            switch (option)
            {
                case "1":
                    emailAction.CreateNewEmail();
                    break;
                case "2":
                    emailAction.CreateFileXml();
                    break;
                case "3":
                    emailAction.ReadFileXml();
                    break;
                case "4":
                    Environment.Exit(0);
                    break;
                default:
                    Console.WriteLine("Invalid option. Please try again.");
                    break;
            }
        }
    }

    //static void SortAndDisplayEmailList()
    //{
    //    // Sort the email list by subject
    //    emailList = emailList.OrderBy(email => email.Subject).ToList();

    //    // Display the sorted email list
    //    foreach (var email in emailList)
    //    {
    //        Console.WriteLine($"From: {email.From}");
    //        Console.WriteLine($"To: {string.Join(", ", email.To)}");
    //        Console.WriteLine($"Subject: {email.Subject}");
    //        Console.WriteLine();
    //    }
    //}
}