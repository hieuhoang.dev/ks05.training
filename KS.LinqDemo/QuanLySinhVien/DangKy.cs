﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLySinhVien
{
    internal class DangKy
    {
        public int MaSinhVien { get; set; }
        public int MaMonHoc { get; set; }
        public float Diem { get; set; }
    }
}
