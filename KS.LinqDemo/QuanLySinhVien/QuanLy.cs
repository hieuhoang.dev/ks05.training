﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Threading.Tasks;

namespace QuanLySinhVien
{
    internal class QuanLy
    {
        //List<SinhVien> danhSachSinhVien = new List<SinhVien>();
        //List<MonHoc> danhSachMonHoc = new List<MonHoc>();
        //List<DangKy> danhSachDangKy = new List<DangKy>();

        List<SinhVien> danhSachSinhVien = new List<SinhVien>
        {
            new SinhVien { MaSinhVien = 1, HoTen = "Nguyễn Văn A", Tuoi = 20, Khoa = "Công nghệ thông tin" },
            new SinhVien { MaSinhVien = 2, HoTen = "Trần Thị B", Tuoi = 22, Khoa = "Toán học" },
            new SinhVien { MaSinhVien = 3, HoTen = "Lê Văn C", Tuoi = 21, Khoa = "Công nghệ thông tin" },
            new SinhVien { MaSinhVien = 4, HoTen = "Phạm Văn D", Tuoi = 23, Khoa = "Vật lý" },
            new SinhVien { MaSinhVien = 5, HoTen = "Hoàng Thị E", Tuoi = 24, Khoa = "Toán học" }
        };

        List<MonHoc> danhSachMonHoc = new List<MonHoc>
        {
            new MonHoc { MaMonHoc = 101, TenMonHoc = "Lập trình cơ bản" },
            new MonHoc { MaMonHoc = 102, TenMonHoc = "Toán cao cấp" },
            new MonHoc { MaMonHoc = 103, TenMonHoc = "Vật lý cơ bản" }
        };

        List<DangKy> danhSachDangKy = new List<DangKy>
        {
            new DangKy { MaSinhVien = 1, MaMonHoc = 101, Diem = 10 },
            new DangKy { MaSinhVien = 1, MaMonHoc = 102, Diem = 9 },
            new DangKy { MaSinhVien = 2, MaMonHoc = 101, Diem = 8 },
            new DangKy { MaSinhVien = 3, MaMonHoc = 103, Diem = 7 },
            new DangKy { MaSinhVien = 4, MaMonHoc = 102, Diem = 6 }
            //new DangKy { MaSinhVien = 5, MaMonHoc = 103, Diem = 5 }
        };


        public void TaoSinhVien()
        {
            //SinhVien sinhVien = new SinhVien();
            //int mSV = 0;
            //var countSV = 0;
            //do
            //{
            //    Console.WriteLine("Mời bạn nhập mã sinh viên");
            //    mSV = Int32.Parse(Console.ReadLine());
            //    countSV = danhSachSinhVien.Where(x => x.MaSinhVien == mSV).Count();
            //} while (countSV > 0);
            //sinhVien.MaSinhVien = mSV;
            //Console.WriteLine("Mời bạn nhập tên sinh viên");
            //sinhVien.HoTen = Console.ReadLine();
            //Console.WriteLine("Mời bạn nhập tuổi sinh viên");
            //sinhVien.Tuoi = Int32.Parse(Console.ReadLine());
            //Console.WriteLine("Mời bạn nhập khoa");
            //sinhVien.Khoa = Console.ReadLine();
            //danhSachSinhVien.Add(sinhVien);
            //Console.WriteLine("Bạn có muốn nhập tiếp hay không?(Y/N) ");
            //string check = Console.ReadLine();
            //if (check.ToLower().Equals("y"))
            //{
            //    TaoSinhVien();
            //}
            try
            {
                Console.WriteLine("Mời bạn nhập mã sinh viên");
                int ma = Int32.Parse(Console.ReadLine());
            }
            catch (Exception ex) { 
                ex.ToString();
            }
        }

        public void TaoMonHoc()
        {
            MonHoc monHoc = new MonHoc();

            int mMH = 0;
            var countMH = 0;
            do
            {
                Console.WriteLine("Mời bạn nhập mã môn học");
                mMH = Int32.Parse(Console.ReadLine());
                countMH = danhSachMonHoc.Where(x => x.MaMonHoc == mMH).Count();
            } while (countMH > 0);
            monHoc.MaMonHoc = mMH;
            Console.WriteLine("Mời bạn nhập tên môn học");
            monHoc.TenMonHoc = Console.ReadLine();
            danhSachMonHoc.Add(monHoc);
            Console.WriteLine("Bạn có muốn nhập tiếp hay không?(Y/N) ");
            string check = Console.ReadLine();
            if (check.ToLower().Equals("y"))
            {
                TaoMonHoc();
            }
        }

        public void DangKyMonHoc()
        {
            DangKy dangKy = new DangKy();
            int checkSv = 0;
            int maSV = 0;
            int checkMH = 0;
            int maMH = 0;
            
            do
            {
                Console.WriteLine("Nhập mã sinh viên: ");
                maSV = Int32.Parse(Console.ReadLine());
                checkSv = danhSachSinhVien.Where(x => x.MaSinhVien == maSV).Count();

            } while (checkSv == 0);
            do
            {
                Console.WriteLine("Nhập mã môn học: ");
                maMH = Int32.Parse(Console.ReadLine());
                checkMH = danhSachMonHoc.Where(x => x.MaMonHoc == maMH).Count();

            } while (checkSv == 0) ;

            int checkDuyNhat = danhSachDangKy
                                .Where(x => (x.MaMonHoc == maMH) && (x.MaSinhVien == maSV))
                                .Count();
            if (checkDuyNhat == 1)
            {
                Console.WriteLine("Sinh viên đã đăng ký môn học");
                return;
            }

            dangKy.MaSinhVien = maSV;
            dangKy.MaMonHoc = maMH;
            Console.WriteLine("Nhập điểm sinh viên: ");
            dangKy.Diem = float.Parse(Console.ReadLine());
            danhSachDangKy.Add(dangKy);
            Console.WriteLine("Bạn có muốn nhập tiếp hay không?(Y/N) ");
            string check = Console.ReadLine();
            if (check.ToLower().Equals("y"))
            {
                DangKyMonHoc();
            }
        }

        public void ThongKeDanhSachSinhVienVaMonHoc()
        {
            //var thongKeDanhSachSinhVienVaMonHoc = from dk in danhSachDangKy
            //                                      join sv in danhSachSinhVien on dk.MaSinhVien equals sv.MaSinhVien
            //                                      group dk by sv.HoTen into NhomTenSV
            //                                      select new
            //                                      {
            //                                          SV = NhomTenSV.Key,
            //                                          TongMonHoc = NhomTenSV.Count(),
            //                                      };
            //foreach (var sv in thongKeDanhSachSinhVienVaMonHoc)
            //{
            //    Console.WriteLine("Sinh vien: " + sv.SV + " | So luong mon hoc: " + sv.TongMonHoc);
            //}

            //var query = from d in danhSachSinhVien
            //            join e in danhSachDangKy
            //            on d.MaSinhVien equals e.MaSinhVien into ed
            //            //from e in ed.DefaultIfEmpty()
            //            select new
            //            {
            //                MaMH = d.MaSinhVien,
            //                SoLuong = ed.Count()
            //            };

            //foreach (var result in query)
            //{
            //    Console.WriteLine($"Sinh vien {result.MaMH} va so luong mon hoc {result.SoLuong}");
            //}


            var testJoin = from sv in danhSachSinhVien
                           join dk in danhSachDangKy on sv.MaSinhVien equals dk.MaSinhVien
                           into groupSinhVien
                           from g in groupSinhVien.DefaultIfEmpty()
                           select new
                           {
                               MaMonHoc = g == null ? 0 : g.MaMonHoc,
                               TenSinhVien = sv.HoTen
                           };

            //foreach ( var t in testJoin )
            //{
            //    Console.WriteLine($"Sinh vien {t.TenSinhVien} va ma mon hoc {t.MaMonHoc}");
            //}

            var test = from sv in testJoin
                                        group sv by sv.TenSinhVien into groupTen
                                        select new
                                        {
                                            TenSinhVien = groupTen.Key,
                                            MonHoc = groupTen,
                                        };

            foreach ( var t in test)
            {
                Console.WriteLine($"Sinh vien {t.TenSinhVien}");
                int totalMonHoc = 0;
                foreach (var s in t.MonHoc)
                {
                    if(s.MaMonHoc > 0)
                    {
                        totalMonHoc++;
                    }
                }
                Console.WriteLine($"So mon hoc: " + totalMonHoc);
            }

        }

        public void ThongKeDanhMonHocVaSoSinhVien()
        {
            var thongKeDanhMonHocVaSoSinhVien = from dk in danhSachDangKy
                                          group dk by dk.MaMonHoc into NhomMaMH
                                          select new
                                          {
                                              MH = NhomMaMH.Key,
                                              TongSinhVien = NhomMaMH.Count(),
                                          };
            foreach (var mh in thongKeDanhMonHocVaSoSinhVien)
            {
                Console.WriteLine("Mon hoc: " + mh.MH + " | So luong sinh vien: " + mh.TongSinhVien);
            }
        }

        public void ThongKeTongSoLuongSinhVien()
        {
            int countSV = danhSachSinhVien.Count();
            Console.WriteLine("Tổng số sinh viên: " + countSV);
        }

        public  void ThongKeMonHocCoSinhVien()
        {
            var thongKeMonHocCoSinhVien = from dk in danhSachDangKy
                                                  group dk by dk.MaMonHoc into NhomMaMH
                                                  select new
                                                  {
                                                      MH = NhomMaMH.Key,
                                                      TongSinhVien = NhomMaMH.Count(),
                                                  };

            Console.WriteLine("So mon hoc co sinh vien: " + thongKeMonHocCoSinhVien.Count());
        }

        public void ThongKeMonHocKhongCoSinhVien()
        {
            var thongKeMonHocCoSinhVien = from dk in danhSachDangKy
                                          group dk by dk.MaMonHoc into NhomMaMH
                                          select new
                                          {
                                              MH = NhomMaMH.Key,
                                              TongSinhVien = NhomMaMH.Count(),
                                          };
            Console.WriteLine("So mon hoc khong co sinh vien: " + (danhSachMonHoc.Count() - thongKeMonHocCoSinhVien.Count()));
        }
    }
}
