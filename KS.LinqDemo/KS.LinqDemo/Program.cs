﻿using KS.LinqDemo;
using System.ComponentModel.Design.Serialization;
using System.Text;

Console.OutputEncoding = Encoding.Unicode;
List<SinhVien> sinhViens = new List<SinhVien>
        {
            new SinhVien { MaSinhVien = 1, HoTen = "Nguyễn Văn A", Tuoi = 20, Khoa = "Công nghệ thông tin" },
            new SinhVien { MaSinhVien = 2, HoTen = "Trần Thị B", Tuoi = 22, Khoa = "Toán học" },
            new SinhVien { MaSinhVien = 3, HoTen = "Lê Văn C", Tuoi = 21, Khoa = "Công nghệ thông tin" },
            new SinhVien { MaSinhVien = 4, HoTen = "Phạm Văn D", Tuoi = 23, Khoa = "Vật lý" },
            new SinhVien { MaSinhVien = 5, HoTen = "Hoàng Thị E", Tuoi = 24, Khoa = "Toán học" }
        };

List<MonHoc> monHocs = new List<MonHoc>
        {
            new MonHoc { MaMonHoc = 101, TenMonHoc = "Lập trình cơ bản" },
            new MonHoc { MaMonHoc = 102, TenMonHoc = "Toán cao cấp" },
            new MonHoc { MaMonHoc = 103, TenMonHoc = "Vật lý cơ bản" }
        };

List<GhiDanh> ghiDanhs = new List<GhiDanh>
        {
            new GhiDanh { MaSinhVien = 1, MaMonHoc = 101 },
            new GhiDanh { MaSinhVien = 1, MaMonHoc = 102 },
            new GhiDanh { MaSinhVien = 2, MaMonHoc = 101 },
            new GhiDanh { MaSinhVien = 3, MaMonHoc = 103 },
            new GhiDanh { MaSinhVien = 4, MaMonHoc = 102 },
            new GhiDanh { MaSinhVien = 5, MaMonHoc = 103 }
        };


var dsMonHoc = from m in monHocs
               where m.MaMonHoc == 101
               orderby m.TenMonHoc
               select m;

foreach (var d in dsMonHoc)
{
    Console.WriteLine(d.MaMonHoc + "-" + d.TenMonHoc);
}

var listMonHoc = monHocs.Select(m => ( m.TenMonHoc, m.MaMonHoc));
MonHoc monHoc = new MonHoc();
monHoc.TenMonHoc = listMonHoc.FirstOrDefault().TenMonHoc;
monHoc.MaMonHoc = listMonHoc.FirstOrDefault().MaMonHoc;

//MonHoc monHoc2 = new MonHoc();
//monHoc2.TenMonHoc = listMonHoc.First().TenMonHoc;
//monHoc2.MaMonHoc = listMonHoc.First().MaMonHoc;

//MonHoc monHoc3 = new MonHoc();
//monHoc3.TenMonHoc = listMonHoc.SingleOrDefault().TenMonHoc;
//monHoc3.MaMonHoc = listMonHoc.Single().MaMonHoc;
foreach (var d in listMonHoc)
{
    Console.WriteLine(d.MaMonHoc + "-" + d.TenMonHoc);
}






Console.WriteLine("-------------------------------");
// LINQ Query để lọc các sinh viên thuộc khoa "Công nghệ thông tin"
var sinhVienCNTTQuery = from sv in sinhViens
                        where sv.Khoa == "Công nghệ thông tin"
                        select sv;

// LINQ Query để tham gia và trích xuất thông tin sinh viên và môn học
var thongTinSinhVienQuery = from sv in sinhViens
                            join gd in ghiDanhs on sv.MaSinhVien equals gd.MaSinhVien
                            join mh in monHocs on gd.MaMonHoc equals mh.MaMonHoc
                            select new
                            {
                                sv.HoTen,
                                mh.TenMonHoc
                            };

foreach (var t in thongTinSinhVienQuery)
{
    Console.WriteLine(t.HoTen + "-" + t.TenMonHoc);
}
Console.WriteLine("--------------------------");
// LINQ Query để trích xuất danh sách tên sinh viên
var tenSinhVienQuery = from sv in sinhViens
                       select sv.HoTen;

// LINQ Query để sắp xếp sinh viên theo tuổi tăng dần
var sinhVienSapXepQuery = from sv in sinhViens
                          orderby sv.Tuoi
                          select sv;

// LINQ Query để nhóm sinh viên theo khoa học
var sinhVienTheoKhoaQuery = from sv in sinhViens
                            group sv by sv.Khoa into khoaGroup
                            select new
                            {
                                Khoa = khoaGroup.Key,
                                SinhViens = khoaGroup,
                            };
Console.WriteLine("**************");
foreach (var sv in sinhVienTheoKhoaQuery)
{
    Console.WriteLine($"Khoa: {sv.Khoa}");
    foreach (var s in sv.SinhViens)
    {
        Console.WriteLine($"\tSinhVien: {s.MaSinhVien}, Price: {s.HoTen}");
    }
}
Console.WriteLine("**************");

// LINQ Query để nối danh sách sinh viên thuộc khoa "Vật lý" và "Toán học"
var sinhVienLyThuyetQuery = (from sv in sinhViens
                             where sv.Khoa == "Vật lý"
                             select sv).Concat(from sv in sinhViens
                                               where sv.Khoa == "Toán học"
                                               select sv);

// LINQ Query để tính tuổi trung bình của tất cả sinh viên
var tuoiTrungBinhQuery = sinhViens.Average(sv => sv.Tuoi);

// LINQ Query để đếm tổng số sinh viên
var tongSinhVienQuery = sinhViens.Count();

// LINQ Query để kiểm tra xem có sinh viên nào có tuổi lớn hơn 25 không
var coSinhVienLonHon25TuoiQuery = sinhViens.Any(sv => sv.Tuoi > 25);

// Toán tử lọc
//Biểu thức sv => sv.Khoa == "Công nghệ thông tin" là một biểu thức lambda trong LINQ và được sử dụng để lọc các phần tử trong danh sách dựa trên một điều kiện cụ thể. Hãy cùng giải thích các phần của biểu thức này:

//sv là một biến tham chiếu đến phần tử hiện tại trong danh sách (trong trường hợp này, là một đối tượng SinhVien).

//sv.Khoa truy cập thuộc tính "Khoa" của đối tượng sv, tức là thuộc tính "Khoa" của sinh viên.

//== là toán tử so sánh bằng, nó kiểm tra xem giá trị của thuộc tính "Khoa" có bằng chuỗi "Công nghệ thông tin" hay không.
var sinhVienCNTT = sinhViens.Where(sv => sv.Khoa == "Công nghệ thông tin");

// Toán tử kết hợp (Join)
var thongTinSinhVien = sinhViens
    .Join(ghiDanhs, sv => sv.MaSinhVien, gd => gd.MaSinhVien,
        (sv, gd) => new
        {
            sv.HoTen,
            gd.MaMonHoc
        })
    .Join(monHocs, info => info.MaMonHoc, mh => mh.MaMonHoc,
        (info, mh) => new
        {
            info.HoTen,
            mh.TenMonHoc
        });

// Toán tử chiết xuất (Projection)
var tenSinhVien = sinhViens.Select(sv => sv.HoTen);

// Toán tử sắp xếp
var sinhVienSapXep = sinhViens.OrderBy(sv => sv.Tuoi);

// Toán tử nhóm (Grouping)
var sinhVienTheoKhoa = sinhViens.GroupBy(sv => sv.Khoa);

// Toán tử chuyển đổi (Conversion)
var mangSinhVien = sinhViens.ToArray();
var danhSachSinhVien = sinhViens.ToList();

// Toán tử nối (Concatenation)
var sinhVienLyThuyet = sinhViens.Where(sv => sv.Khoa == "Vật lý")
    .Concat(sinhViens.Where(sv => sv.Khoa == "Toán học"));

// Toán tử tổng hợp (Aggregation)
var tuoiTrungBinh = sinhViens.Average(sv => sv.Tuoi);
var tongSinhVien = sinhViens.Count();

// Toán tử kiểm tra (Quantifier)
var coSinhVienLonHon25Tuoi = sinhViens.Any(sv => sv.Tuoi > 25);

// Toán tử chia thành phần (Partition)
var haiSinhVienDau = sinhViens.Take(2);
var boQuaHaiSinhVien = sinhViens.Skip(2);

// Toán tử tạo (Generation)
var soNguyen = Enumerable.Range(1, 5);

// Toán tử tập hợp (Set)
var sinhVienToanHoc = sinhViens.Where(sv => sv.Khoa == "Toán học");
var sinhVienCNTTVaToanHoc = sinhViens.Where(sv => sv.Khoa == "Công nghệ thông tin")
    .Union(sinhVienToanHoc);

// Toán tử so sánh (Equality)
var bangNhau = sinhViens.SequenceEqual(danhSachSinhVien);

// Toán tử phần tử (Element)
var sinhVienTreNhat = sinhViens.Min(sv => sv.Tuoi);
var sinhVienLonNhat = sinhViens.Max(sv => sv.Tuoi);

// In kết quả
Console.WriteLine("Sinh viên Công nghệ thông tin:");
foreach (var sv in sinhVienCNTT)
{
    Console.WriteLine(sv.HoTen);
}

Console.WriteLine("Thông tin sinh viên:");
foreach (var info in thongTinSinhVien)
{
    Console.WriteLine($"{info.HoTen} - {info.TenMonHoc}");
}

Console.WriteLine("Tên sinh viên:");
Console.WriteLine(string.Join(", ", tenSinhVien));

Console.WriteLine("Sinh viên đã sắp xếp:");
foreach (var sv in sinhVienSapXep)
{
    Console.WriteLine($"{sv.HoTen} - {sv.Tuoi}");
}

Console.WriteLine("Sinh viên theo khoa:");
foreach (var group in sinhVienTheoKhoa)
{
    Console.WriteLine($"Khoa: {group.Key}");
    foreach (var sv in group)
    {
        Console.WriteLine(sv.HoTen);
    }
}

Console.WriteLine("Tuổi trung bình: " + tuoiTrungBinh);
Console.WriteLine("Tổng số sinh viên: " + tongSinhVien);
Console.WriteLine("Có sinh viên nào lớn hơn 25 tuổi không: " + coSinhVienLonHon25Tuoi);

Console.WriteLine("Hai sinh viên đầu:");
foreach (var sv in haiSinhVienDau)
{
    Console.WriteLine(sv.HoTen);
}

Console.WriteLine("Bỏ qua hai sinh viên:");
foreach (var sv in boQuaHaiSinhVien)
{
    Console.WriteLine(sv.HoTen);
}

Console.WriteLine("Dãy số: " + string.Join(", ", soNguyen));

Console.WriteLine("Sinh viên Toán học và Công nghệ thông tin:");
foreach (var sv in sinhVienCNTTVaToanHoc)
{
    Console.WriteLine(sv.HoTen);
}

Console.WriteLine("Có bằng nhau: " + bangNhau);
Console.WriteLine("Sinh viên trẻ nhất: " + sinhVienTreNhat);
Console.WriteLine("Sinh viên lớn nhất: " + sinhVienLonNhat);