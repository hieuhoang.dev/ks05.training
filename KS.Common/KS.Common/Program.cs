﻿// See https://aka.ms/new-console-template for more information
using KS.Common;
using System.ComponentModel.Design;
using System.Xml.Linq;

Console.OutputEncoding = System.Text.Encoding.Unicode;

ParamsDemo paramsDemo = new ParamsDemo();

List<Person> person = new List<Person>();

int[] a = { 1, 3, 4 };
string[] b = { "a" };

//object[] c = new object[a.Length + b.Length];


//paramsDemo.Sum(a, b);
//int result1 = paramsDemo.Sum(1, "a", true); // Số 1, 2, 3 được truyền dưới dạng mảng int.
//Console.WriteLine("Tổng của các số là: " + result1); // Kết quả: 6

//int result2 = paramsDemo.Sum(5, 10, 15, 20); // Số 5, 10, 15, 20 được truyền dưới dạng mảng int.
//Console.WriteLine("Tổng của các số là: " + result2); // Kết quả: 50








ArgumentDemo argumentDemo = new ArgumentDemo();

//Named arguments cho phép bạn gọi hàm bằng cách chỉ định tên của tham số mà bạn muốn truyền, 
//thay vì theo thứ tự.
//Điều này giúp rõ ràng hơn và tránh nhầm lẫn khi có nhiều tham số.
// Sử dụng named arguments
argumentDemo.DisplayInfoNamedArguments(age: 30, name: "John");

//Optional arguments là khả năng xác định giá trị mặc định cho một hoặc nhiều tham số trong một phương thức.
//Khi gọi phương thức, bạn có thể bỏ qua tham số có giá trị mặc định và hàm sẽ sử dụng giá trị mặc định đã được xác định trước.
// Gọi phương thức với đối số tùy chọn (age)
argumentDemo.DisplayInfoOptionalArguments("Alice"); // Hiển thị Age: 25
argumentDemo.DisplayInfoOptionalArguments("Bob", 30); // Hiển thị Age: 30

Console.WriteLine(argumentDemo.Display(2));

decimal dec = 0;
if(dec == 5.1M)
{
    Console.WriteLine("Okela");
}

string original = "hello";
string reversed = original.Reverse(); // Gọi extension method
Console.WriteLine("Original: " + original);
Console.WriteLine("Reversed: " + reversed);


//Lambda expressions là một tính năng quan trọng trong C#, cho phép định nghĩa một phương thức (method) ngắn gọn và linh hoạt mà không cần phải viết một phương thức riêng biệt. 
//Lambda expressions thường được sử dụng khi bạn cần truyền một hàm (delegate) như một tham số hoặc thực hiện các xử lý ngắn gọn và đơn giản.

// Lambda expression để tính bình phương
Func<int, int> square = x => x * x;

// Gọi lambda expression
int result = square(5);
Console.WriteLine("Bình phương của 5 là: " + result);  // Kết quả: 25

// Lambda expression để in chuỗi "Hello, world!"
Action printHello = () => Console.WriteLine("Hello, world!");
printHello();  // Kết quả: "Hello, world!"

// Lambda expression có tham số
Action<string> greet = (name) => Console.WriteLine("Hello, " + name + "!");
greet("Alice");  // Kết quả: "Hello, Alice!"

// Lambda expression với nhiều tham số và biểu thức phức tạp
Func<int, int, int> add = (a, b) =>
{
    int sum = a + b;
    return sum;
};

int sumResult = add(10, 20);
Console.WriteLine("10 + 20 = " + sumResult);  // Kết quả: "10 + 20 = 30"