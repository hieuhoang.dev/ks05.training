﻿using System;
using System.Text;

internal class Program
{
    private static void Main(string[] args)
    {
        Console.OutputEncoding = Encoding.UTF8;
        try
        {
            // Trường hợp 1: Divide by zero exception
            int numerator = 10;
            int denominator = 0;
            int result = numerator / denominator;
        }
        catch (DivideByZeroException ex)
        {
            Console.WriteLine("Lỗi chia cho 0: " + ex.Message);
        }

        try
        {
            // Trường hợp 2: Index out of range exception
            int[] numbers = { 1, 2, 3 };
            int index = 3;
            int value = numbers[index];
        }
        catch (IndexOutOfRangeException ex)
        {
            Console.WriteLine("Lỗi vượt quá phạm vi của mảng: " + ex.Message);
        }

        //try
        //{
        //    // Trường hợp 3: Null reference exception
        //    string text = null;
        //    int length = text.Length;
        //}
        //catch (NullReferenceException ex)
        //{
        //    Console.WriteLine("Lỗi tham chiếu null: " + ex.Message);
        //}

        try
        {
            // Trường hợp 4: Custom exception
            throw new CustomException("Lỗi xử lý tùy chỉnh xảy ra");
        }
        catch (CustomException ex)
        {
            Console.WriteLine("Lỗi tùy chỉnh: " + ex.Message);
        }

        try
        {
            // Thử mở và đọc một tệp
            string filePath = "sample.txt";
            ReadFile(filePath);
        }
        catch (FileNotFoundException ex)
        {
            Console.WriteLine("Không tìm thấy tệp: " + ex.Message);
        }
        catch (Exception ex)
        {
            Console.WriteLine("Lỗi khác: " + ex.Message);
        }
        finally
        {
            // Mã trong khối finally sẽ luôn được thực thi, ngay cả khi có exception
            Console.WriteLine("Khối finally được thực thi bất kể có lỗi hay không.");
        }

        // Thử chia 10 cho 2
        int result1 = DivideNumbers(10, 2);
        Console.WriteLine("Kết quả: " + result1);

        // Thử chia 10 cho 0
        int result2 = DivideNumbers(10, 0);
        Console.WriteLine("Kết quả: " + result2);

    }

    static void ReadFile(string filePath)
    {
        try
        {
            // Mở và đọc tệp
            // Simulate a FileNotFoundException
            throw new FileNotFoundException("Tệp không tồn tại.");
        }
        catch (FileNotFoundException ex)
        {
            Console.WriteLine("Lỗi: " + ex.Message);
            // Rethrow exception để nó có thể được bắt ở mức cao hơn
            throw;
        }
        finally
        {
            // Mã trong khối finally sẽ được thực thi trước khi exception được ném ra
            Console.WriteLine("Khối finally trong ReadFile được thực thi.");
        }
    }

    public static int DivideNumbers(int numerator, int denominator)
    {
        //try-catch:

        //try-catch là một cấu trúc kiểm soát luồng(control flow) trong ngôn ngữ lập trình, được sử dụng để bắt và xử lý exception.
        //try là nơi bạn đặt mã mà bạn muốn kiểm tra xem có exception xảy ra không.
        //catch là nơi bạn đặt mã xử lý exception, và bạn có thể xác định loại exception bạn muốn bắt.
        //Khi một exception xảy ra trong khối try, quá trình thực hiện mã trong khối try dừng lại và chuyển sang khối catch tương ứng để xử lý exception.
        //try-catch cho phép bạn kiểm soát và xử lý exception một cách linh hoạt.
        //throw:

        //throw là một toán tử được sử dụng để ném một exception từ bất kỳ đâu trong mã của bạn.
        //Bạn có thể sử dụng throw để ném exception đã được xây dựng sẵn(như Exception hoặc các lớp exception con) hoặc tự định nghĩa một exception tùy chỉnh.
        //throw cho phép bạn gây ra exception khi một điều kiện không mong muốn xảy ra trong mã.
        //throw không xử lý exception trực tiếp; nó chỉ đơn giản là tạo ra một exception để cho phép mã gọi hoặc khối try-catch bắt nó.
        try
        {
            if (denominator == 0)
            {
                throw new DivideByZeroException("Không thể chia cho 0");
            }
            return numerator / denominator;
        }
        catch (DivideByZeroException ex)
        {
            Console.WriteLine("Lỗi: " + ex.Message);
            return -1; // Giá trị trả về khi có lỗi
        }
    }
}

class CustomException : Exception
{
    public CustomException(string message) : base(message)
    {
    }
}