﻿Console.OutputEncoding = System.Text.Encoding.UTF8;
//int count = 0;

//start:

//Console.WriteLine("Số lần lặp: " + count);

//count++;

//if (count < 5)
//{
//    goto start; // Chuyển đến nhãn "start"
//}

//Console.WriteLine("Kết thúc chương trình.");

mainMenu:

Console.WriteLine("----- Main Menu -----");
Console.WriteLine("1. Xem thông tin");
Console.WriteLine("2. Chỉnh sửa thông tin");
Console.WriteLine("3. Thoát");

Console.Write("Nhập lựa chọn của bạn: ");
int choice = int.Parse(Console.ReadLine());

switch (choice)
{
    case 1:
        Console.WriteLine("Bạn đã chọn xem thông tin.");
        // Thực hiện xem thông tin ở đây
        goto mainMenu;
    case 2:
        Console.WriteLine("Bạn đã chọn chỉnh sửa thông tin.");
        // Thực hiện chỉnh sửa thông tin ở đây
        goto mainMenu;
    case 3:
        Console.WriteLine("Chương trình kết thúc.");
        break;
    default:
        Console.WriteLine("Lựa chọn không hợp lệ. Vui lòng chọn lại.");
        goto mainMenu;
}
