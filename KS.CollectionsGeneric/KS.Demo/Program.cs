﻿using System.Text;

namespace KS.Demo
{
    internal class Program
    {
        delegate int MyDelegate(string s);
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;

            MyDelegate convertToInt = new MyDelegate(ConvertStringToInt);

            MyDelegate showString = new MyDelegate(ShowString);

            MyDelegate multicast = convertToInt + showString;

            string numberSTR = "1309";

            Console.WriteLine("In kết quả: " + multicast(numberSTR));

            int valueConverted = convertToInt(numberSTR);

            Console.WriteLine("Giá trị đã convert thành int: " + valueConverted);

            Console.WriteLine("Kết quả khi gọi multicast Delegate");
            multicast(numberSTR);

            Console.ReadLine();
        }

        static int ConvertStringToInt(string stringValue)
        {
            int valueInt = 0;

            Int32.TryParse(stringValue, out valueInt);
            Console.WriteLine("Đã ép kiểu dữ liệu thành công");

            return valueInt;
        }

        static int ShowString(string stringValue)
        {
            Console.WriteLine(stringValue);
            return 0;
        }
    }
        
}