﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KS.CollectionsGeneric
{
    public class MyCollection
    {
        private int[] numbers;

        public MyCollection(int[] nums)
        {
            numbers = nums;
        }

        public IEnumerator GetEnumerator()
        {
            for (int i = 0; i < numbers.Length; i++)
            {
                yield return numbers[i];
            }
        }
    }
}
