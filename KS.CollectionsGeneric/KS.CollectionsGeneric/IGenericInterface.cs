﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KS.CollectionsGeneric
{
    internal interface IGenericInterface<T>
    {
        T GetData();
    }
}
