﻿// See https://aka.ms/new-console-template for more information
using KS05;
using System;
using System.Text;

Console.OutputEncoding = Encoding.UTF8;
//Console.Write("Hello, World!");
//Console.WriteLine("Bài học của lớp KS");

//string stg = "Lớp KS05";
//string stg5 = "Lớp KS05";

//String stg2 = stg;
//String stg4 = stg5;
//Console.WriteLine(stg2.Equals(stg4));

//string stg3 = stg2;


//Student student = new Student();
//student.Id = 1;
//student.Name = "Hiếu";

//Console.WriteLine(student.Id + "-" + student.Name);

//Student student2 = new Student();
//student2.Id = 1;
//student2.Name = "Hiếu";
//Console.WriteLine(student2.Id + "-" + student2.Name);

//Console.WriteLine(student == student2);
//Console.WriteLine(student.Equals(student2));

//==
//Hai đối tượng student và student2 đều được tạo mới và có tham chiếu khác nhau (không cùng trỏ đến cùng một vị trí bộ nhớ).
//Toán tử == mặc định so sánh tham chiếu của đối tượng, và do hai đối tượng này có tham chiếu khác nhau, nên kết quả là false.


//Equal
//Hai đối tượng student và student2 đều được tạo mới và có tham chiếu khác nhau (không cùng trỏ đến cùng một vị trí bộ nhớ).
//Mặc dù các thuộc tính Id và Name của student và student2 có giá trị giống nhau, nhưng phương thức Equals chưa được ghi đè, vì vậy mặc định nó sẽ so sánh tham chiếu.
//Do tham chiếu khác nhau, kết quả của Equals sẽ là false.



//Bạn đang so sánh hai chuỗi (student.Name và student2.Name) bằng toán tử ==, nó so sánh giá trị của hai chuỗi.
//Cả hai chuỗi đều có giá trị là "Hiếu", nên kết quả của student.Name == student2.Name là true.
//Tương tự, khi sử dụng phương thức Equals để so sánh hai chuỗi, kết quả cũng là true vì nó so sánh giá trị của hai chuỗi.


//== là toán tử so sánh dùng để kiểm tra bằng nhau giữa các kiểu dữ liệu nguyên thủy hoặc tham chiếu (kiểm tra tham chiếu đến cùng một vị trí bộ nhớ).
//Equals là phương thức được gọi để so sánh đối tượng theo quy tắc được định nghĩa trong lớp (có thể ghi đè để xác định cách so sánh theo ý muốn).
//StudentKS05 studentKS05 = new StudentKS05();
//studentKS05.Test();

//String[] arr = new String[10];

//Console.WriteLine(arr[7] == arr[8]);
//for (int i = 0; i < arr.Length; i++)
//{
//    Console.WriteLine(arr[i]);
//}

//int[] arr2 = { 1, 2, 3 };
//int[] arr3;
//arr3 = new int[10];


//int[,] arr4 = new int[10,3];
//int[][] arr5 = new int[10][];

//const int MAX = 10;

//int a = 10;
//int b = 20;
//++a;
//a++;
//--a;
//a--;
//b++;
//b += a;
//int c = a + ++a + b;
//Console.WriteLine(c);

//String a = new String("Hello CodeGym");

//String b = new String("Hello CodeGym");

//String c = a;

//Console.WriteLine(a == b);

//Console.WriteLine(a == c);

//Console.WriteLine(a.Equals(b));

//Console.WriteLine(a.Equals(c));

//Day2
int a = 10;
bool c = true;

if (a >= 10)
{
    int b = 20;
    if (b > 20) { }
}
else if (c == false)
{
    Console.WriteLine("Test");
}
else
{

}

if (c == true)
{
    Console.WriteLine("Test");
}
Console.WriteLine(c);
int month = 1;
switch (month)
{
    case 1:
    case 3:
        Console.WriteLine("Tháng 31 ngày");
        break;
    case 2:
        Console.WriteLine("Tháng 28(29) ngày");
        break;
    case 4:
        Console.WriteLine("Tháng 30 ngày");
        break;
    default:
        Console.WriteLine("Không có tháng nào");
        break;
}

if(month == 1 || month == 3)
{
    Console.WriteLine("Tháng 31 ngày");
}


void PrintStudent()
{
    return;
}

PrintStudent();
//Console.WriteLine(printStudent().Item1);
//Console.WriteLine(printStudent().Item2);
while (true)
{
    break;
}